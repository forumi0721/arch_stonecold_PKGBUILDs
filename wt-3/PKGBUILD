# $Id$
# Maintainer: Sergej Pupykin <pupykin.s+arch@gmail.com>
# Maintainer: Sven-Hendrik Haase <sh@lutzhaase.com>
# Contributor: Denis Martinez <deuns.martinez@gmail.com>

_basename=wt
pkgname=${_basename}-3
pkgver=3.3.10
pkgrel=1
pkgdesc="a C++ library and application server for developing and deploying web applications"
arch=('armv6h' 'armv7h' 'x86_64')
url="http://www.webtoolkit.eu/"
license=('GPL')
depends=('boost-libs' 'libharu' 'graphicsmagick' 'pango' 'zlib')
makedepends=('boost' 'cmake' 'postgresql-libs' 'fcgi' 'sqlite' 'mysql++' 'qt4' 'doxygen' 'mesa' 'glu')
optdepends=('openssl: for SSL support in built-in webserver'
	    'fcgi: for FastCGI support'
	    'postgresql-libs: for PostgreSQL Dbo support'
	    'sqlite: for Sqlite Dbo support'
	    'mysql++: for the hangman example'
	    'qt4: for the Wt/Qt interopability example (wtwithqt)')
backup=('etc/wt/wt_config.xml')
source=("${_basename}-$pkgver.tar.gz::https://github.com/kdeforche/wt/archive/${pkgver}.tar.gz")
sha512sums=('cd8c236ba6b48a6c0dfd3924e700185083959478d102aabb617b9f9b56948966d08157c4c21c14337fd920043cef9c9b07f37b750d3cfc632875e467a9abdec5')

build() {
  cd "$srcdir"/${_basename}-${pkgver}

  [[ -d build ]] && rm -r build
  mkdir -p build && cd build
  cmake \
      -DCONNECTOR_HTTP=ON \
      -DWT_CPP_11_MODE=-std=c++11 \
      -DWT_WRASTERIMAGE_IMPLEMENTATION=GraphicsMagick \
      -DCMAKE_INSTALL_PREFIX=/usr \
      -DWEBUSER=http \
      -DWEBGROUP=http \
      -DUSE_SYSTEM_SQLITE3=ON \
      -DINSTALL_EXAMPLES=ON \
      -DBUILD_EXAMPLES=ON \
      -DINSTALL_FINDWT_CMAKE_FILE=ON \
      ..
  make
}

package() {
  cd "$srcdir"/${_basename}-${pkgver}/build

  make DESTDIR="$pkgdir" install
}
