#!/bin/sh

qemu=/usr/bin/sudo-qemu/qemu-arm-static
sudo=/usr/bin/sudo-qemu/sudo
cmd=

if ! ${sudo} -V &> /dev/null ; then
	cmd="${qemu} "
fi

cmd+="${sudo} "

if [ "$(basename "${0}")" = "sudoedit" ]; then
	cmd+="-e "
fi

cmd+="$@"

eval ${cmd}

exit ${?}
